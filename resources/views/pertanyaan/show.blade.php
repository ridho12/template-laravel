@extends('adminLTE.master')

@section('content')
<div class="container">
    <h2>lihat Pertanyaan {{$pertanyaan->id}}</h2>
    <h4>{{$pertanyaan->judul}}</h4>
    <p>{{$pertanyaan->isi}}</p>
</div>
@endsection