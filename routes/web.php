<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('items.index');
});
Route::get('/data-table', function () {
    return view('items.table');
});

Route::get('/master', function () {
    return view('adminLTE.master');
});


Route::get('/welcome', function () {
    return view('welcome');
});


// tugas CRUD
Route::get('/pertanyaan/tambah','pertanyaanController@tambah');
Route::post('/pertanyaan','pertanyaanController@store');
Route::get('/pertanyaan','pertanyaanController@index');
Route::get('/pertanyaan/{id}','pertanyaanController@show');
Route::get('/pertanyaan/{id}/edit','pertanyaanController@edit');
Route::put('/pertanyaan/{id}','pertanyaanController@update');
Route::delete('/pertanyaan/{id}','pertanyaanController@destroy');
